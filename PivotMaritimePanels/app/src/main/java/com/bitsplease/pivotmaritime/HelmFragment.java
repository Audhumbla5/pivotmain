package com.bitsplease.pivotmaritime;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

public class HelmFragment extends Fragment {
    MainActivity ma;
    double progress = 0;
    HelmSlider helmSlider;

    /**
     * layout_joystick is the LinearLayout in fragment_helm.xml that has the background image of
     * the Helm and is instantiated in onViewCreated().
     * It will be what the helmSlider will be rendered on top of.
     */
    LinearLayout layout_joystick;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_helm, container, false);
    }

    /**
     * Once the Fragment view has been created this code executes.
     * When the Helm Sliders background has loaded the helmSlider object has the dimensions
     * of the background passed into it so that it knows where to render the slider knob.
     * Other initializations are made on the slider here such as setting the slider knob size
     * using setStickSize() and centering the knob on the background.
     * We also set up a listener so the the slider can report its value back to this Fragment
     * which can in turn pass that to the MainActivity if needed.
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layout_joystick = (LinearLayout)getView().findViewById(R.id.helm);
        helmSlider = new HelmSlider(getActivity(), layout_joystick, R.drawable.helmbutton);
        helmSlider.registerObserver(this);

        ViewTreeObserver viewTreeObserver = layout_joystick.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    layout_joystick.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    helmSlider.passXY(layout_joystick.getWidth(),layout_joystick.getHeight());
                    helmSlider.setLeftBoundary(0.09);
                    helmSlider.setRightBoundary(0.9);
                    helmSlider.setStickSize(layout_joystick.getHeight()/2,layout_joystick.getHeight()/3);
                    helmSlider.center();
                    layout_joystick.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View arg0, MotionEvent arg1) {
                            helmSlider.drawStick(arg1);
                            return true;
                        }

                    });
                }
            });
        }
    }
	
	/**
	 * HelmSlider class calls this method when there is a change of progress
	 * It then changes the value of the slider
	 * This method will then call the notifyObserver method
	 *
	 * @param progress the new value of the slider
	 */
    
    public void helmSliderUpdate(double progress){
        this.progress = progress;
        notifyObserver();
    }
	
	/**
	 * Creates a MainActivity in the class
	 *
	 * @param ma main activity object
	 */    
    public void registerObserver(MainActivity ma) {
        this.ma = ma;
    }
	
	/**
	 * Notifies the Main activity that a change has been made to the value of the slider 
	 */
    public void notifyObserver() {
        ma.helmSliderUpdate(progress);
    }
    
	/**
	 * Centers the value of the slider
	 */
    public void center(){
        helmSlider.center();
    }
}