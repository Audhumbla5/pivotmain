package com.bitsplease.pivotmaritime;

import android.app.Fragment;

public abstract class MainPanelFragment extends Fragment {

    private String name = "";
    private State state;
    private MainActivity ma;

    /**
     * Creates a state object that will be the same for all fragments.
     * Used by the main activity when a fragment object is created. The main activity will pass in its state object
     * This way all fragments contain the same state object.
     *
     * @param state the state object that is in the main activity
     */
    public void passInState(State state) {
        this.state = state;
    }

    /**
     * Gives a name to the fragment.
     *
     * @param name The name you would like your fragment to be called
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the name of the fragment
     *
     * @return the name of the fragment
     */
    public String getName(){
        return name;
    }

    /**
     * Returns the state of the fragment
     *
     * @return the state of the fragment
     */
    public State getState() { return state; }

    /**
     *  Returns the main activity
     *
     * @return the main activity object
     */
    public MainActivity getMainActivity() { return ma; }

    /**
     * Gives access to the main activity.
     * When fragments are created by the activity, this is used to provide a reference of the activity to the fragment
     * @param ma The main activity
     */
    public void registerObserver(MainActivity ma) {
        this.ma = ma;
    }
}
