package com.bitsplease.pivotmaritime;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;

public class AdminActivity extends Activity{
    //  Full screen functionality
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHideRunnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            mContentView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            );
        }
    };
    private final Runnable mShowRunnable = new Runnable() {
        @Override
        public void run() {
            mContentView.setVisibility(View.GONE);
        }
    };

    private Button enterMainActivity;
    private CheckBox thrustersCb;
    private CheckBox winchControlCb;
    private CheckBox pointOfViewCb;

    // Added CheckBox variable for example
    private CheckBox checkBoxExample;

    LinkedList<CheckBox> checkBoxList = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  These must be first two methods called
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin);
        //  Full screen functionality
        mContentView = findViewById(R.id.adminmain);
        hide();
    }

    /**
     * Gets the CheckBoxes from layout and puts them into variables and adds them to a LinkedList of
     * CheckBoxes.
     * Also gives the "Enter Simulator" button an onClick method which passes in the state of the CheckBoxes
     * as a bundle to be used in the MainActivity.
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        //  Hide the system UI.
        hide();

        thrustersCb = (CheckBox)findViewById(R.id.thrustersCb);
        checkBoxList.add(thrustersCb);
        winchControlCb = (CheckBox)findViewById(R.id.winchControlCb);
        checkBoxList.add(winchControlCb);
        pointOfViewCb = (CheckBox)findViewById(R.id.pointOfViewCb);
        checkBoxList.add(pointOfViewCb);
        /***
            Adding a checkbox for a new panel called "Example"
         ***/
            //  checkBoxExample = addCheckBox("Example");
            //  checkBoxList.add(checkBoxExample);


        enterMainActivity = (Button)findViewById(R.id.enterMainActivityBtn);
        enterMainActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                if (moreThanOneCheckBoxIsChecked(checkBoxList)) {
                    bundle.putBoolean("thrusters", thrustersCb.isChecked());
                    bundle.putBoolean("winchcontrol", winchControlCb.isChecked());
                    bundle.putBoolean("pointofview", pointOfViewCb.isChecked());
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.putExtras(bundle);
                    startActivity(i);

                } else {

                    Toast.makeText(getApplicationContext(), "Please select at least one panel", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     * Used to determine if more than one of the checkboxes is ticked.
     * This is important as if only one is ticked the arrows to select which panel is where must be disabled
     *
     * @param list The list of checkboxes that the user will see
     * @return true if more than one checkbox is selected, otherwise false
     */
    private boolean moreThanOneCheckBoxIsChecked(LinkedList<CheckBox> list)
    {
        for (CheckBox cb : list)
        {
           if (cb.isChecked())
           {
               return true;
           }
        }
        return false;
    }

    /**
     * Used to hide the system ui
     */
    private void hide() {
        mHideHandler.removeCallbacks(mShowRunnable);
        mHideHandler.postDelayed(mHideRunnable, 0);
    }

    /**
     * A method that can be used to add another checkbox.
     * In the future if another panel fragment is created then a cherckbox can also be added
     * to the admin screen using this methos
     *
     * @param name the text that will appear next to the checkbox. Best to use the name of the panel
     * @return the checkbox that will be added below the last checkbox
     */
    private CheckBox addCheckBox(String name)
    {
        RelativeLayout checkboxContainer = (RelativeLayout) findViewById(R.id.checkboxContainer);
        int id = checkboxContainer.getChildAt(checkboxContainer.getChildCount()-2).getId();
        int titleId = findViewById(R.id.adminTitle).getId();
        CheckBox cb = new CheckBox(this);
        cb.setText(name);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW,id);
        params.addRule(RelativeLayout.ALIGN_LEFT,titleId);
        checkboxContainer.addView(cb,params);
        return cb;
    }

}
