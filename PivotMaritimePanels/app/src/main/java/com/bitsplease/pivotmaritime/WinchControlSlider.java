package com.bitsplease.pivotmaritime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import java.util.ArrayList;
import java.util.List;

public class WinchControlSlider {

    private WinchControlPanelFragment wcpf;
    private Context mContext;
    private ViewGroup mLayout;
    private LayoutParams params;
    private int stick_width, stick_height;
    private int background_width, background_height;
    private double top_boundary, bottom_boundary;
    private double progress;
    private DrawCanvas draw;
    private Paint paint;
    private Bitmap stick;
    private boolean touch_state = false;

    /**
     * Constructor
     * @param context
     * @param layout
     * @param stick_res_id
     */
    public WinchControlSlider(Context context, ViewGroup layout, int stick_res_id) {
        mContext = context;
        stick = BitmapFactory.decodeResource(mContext.getResources(), stick_res_id);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
        draw = new DrawCanvas(mContext);
        paint = new Paint();
        mLayout = layout;
        params = mLayout.getLayoutParams();
        progress = 0.0;
    }

    /**
     * Method used to draw the slider stick onto the slider background.
     * @param arg1 The users touch
     */
    public void drawStick(MotionEvent arg1) {

        if(arg1.getAction() == MotionEvent.ACTION_DOWN) {
            if(arg1.getY()>top_boundary  && arg1.getY()<bottom_boundary) {
                draw.position(arg1.getY());
                draw();
                // progress = -(((arg1.getY()-top_boundary)-0)/(bottom_boundary-top_boundary-0)*200) + 100;
                // ^ explanation below
                // (arg1.getY()-top_boundary)-0)/(bottom_boundary-top_boundary-0) is normalising the value range to 0-1
                // normalising as in (val-min)/(max-min)
                // this is multiplied by 200
                // this gives range of (0 to 200) from top to bottom but we want range of (100 to -100)
                // function f(x)=-x+100 means x = 0 is converted to 100 and x = 200 is converted to -100
                progress = -(((arg1.getY()-top_boundary)-0)/(bottom_boundary-top_boundary-0)*200) + 100;
                notifyObserver();
                touch_state = true;
            }
            else if(arg1.getY()<top_boundary){
                draw.position((int)top_boundary);
                draw();
                progress = -(0/(bottom_boundary-0)*200) + 100;
                notifyObserver();
                touch_state = true;
            }
            else if(arg1.getY()>bottom_boundary){
                draw.position((int)bottom_boundary);
                draw();
                progress = -(((bottom_boundary)-0)/(bottom_boundary-0)*200) + 100;
                notifyObserver();
                touch_state = true;
            }
        } else if(arg1.getAction() == MotionEvent.ACTION_MOVE && touch_state) {
            if(arg1.getY()>top_boundary  && arg1.getY()<bottom_boundary) {
                draw.position(arg1.getY());
                draw();
                progress = -(((arg1.getY()-top_boundary)-0)/(bottom_boundary-top_boundary-0)*200) + 100;
                notifyObserver();
            }
            else if(arg1.getY()<top_boundary){
                draw.position((int)top_boundary);
                draw();
                progress = -( 0/(bottom_boundary-0)*200 ) + 100;
                notifyObserver();
            }
            else if(arg1.getY()>bottom_boundary){
                draw.position((int)bottom_boundary);
                draw();
                progress = -(((bottom_boundary)-0)/(bottom_boundary-0)*200) + 100;
                notifyObserver();
            }
        } else if(arg1.getAction() == MotionEvent.ACTION_UP) {
            center();
            draw();
            touch_state = false;
        }
    }

    /**
     * This method draw the knob onto the screen.
     * Also removes where the knob was before the movement.
     */
    private void draw() {
        try {
            mLayout.removeView(draw);
        } catch (Exception e) { }
        mLayout.addView(draw);
    }

    /**
     * Private class to help with the drawing of slider
     *
     */
    private class DrawCanvas extends View{
        float x, y;

        private DrawCanvas(Context mContext) {
            super(mContext);
        }

        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(stick, x, y, paint);
        }

        private void position(float pos_y) {
            x = getBackgroundWidth()/2 - (stick_width/4);
            y = pos_y - (stick_height / 2);
        }
    }

    /**
     * Used to create the size of the slider knob
     * Usually used by the fragment that contains the slider.
     *
     * @param width the width of the knob
     * @param height the height of the knob
     */
    public void setStickSize(int width, int height) {
        stick = Bitmap.createScaledBitmap(stick, width, height, false);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
    }

    /**
     * Returns background_width
     * @return background_width
     */
    public int getBackgroundWidth(){
        return background_width;
    }

    /**
     * Sets the background width variable of this class from Telegraph Fragment
     * @param background_width
     */
    public void setBackgroundWidth(int background_width) {
        this.background_width = background_width;
    }

    /**
     * Sets the background height variable of this class from Telegraph Fragment
     * @param background_height
     */
    public void setBackgroundHeight(int background_height) {
        this.background_height = background_height;
    }



    /**
     * Given a background you may want to set boundaries for where the stick can be drawn.
     * the y axis of background is counted starting from the top, so at top it is 0 and at bottom
     * it is the value of the background height.
     * if setTopBoundary is set to 0.1 it means a boundary at the top is set at 10% of background height
     * so if background height is 600, then the stick can't be drawn between 0 and 60 on y axis.
     * @param top
     */
    public void setTopBoundary(double top){
        top_boundary = background_height*top;
    }

    /**
     * Same concept as setTopBoundary but would deal with inputs closer to 1.0,
     * like 0.9 to capture bottom boundary.
     * @param bottom
     */
    public void setBottomBoundary(double bottom){
        bottom_boundary = background_height*bottom;
    }


    /**
     * Centers the slider
     *
     */
    public void center(){
        draw.position(background_height/2);
        draw();
    }

    /**
     * Method used to register the slider in the fragment it belongs, the Winch Control Fragment
     * Now it can report progress changes
     *
     * @param wcpf Fragment that houses the slider
     */
    public void registerObserver(WinchControlPanelFragment wcpf) {
        this.wcpf = wcpf;
    }

    /**
     * Notifies the fragment that houses the slider that a progress change has occurred
     *
     */
    public void notifyObserver() {
        wcpf.winchControlSliderUpdate(progress);
    }

}