package com.bitsplease.pivotmaritime;

import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by stewartburnell on 24/08/16.
 */

/**
 * This utility class was created to make the use of a ViewTreeObserver simpler to use.
 * It behaves like a wrapper around the ViewTreeObserver class and results in less code needed to be
 * written with its use.
 *
 * It has one static method.
 *
 * This comes in handy inside of Fragments as we want to wait until the Buttons and other Views
 * inside the Fragment have loaded in order to make use of them.
 *
 * To use: you pass in the View you are waiting for to load and you create an instance of the RunInterface
 * inside the SetUpViewTreeObserver and implement its run method to execute the code you want.
 */

public class ViewTreeObserverUtility {

    public static void SetUpViewTreeObserver (View view, RunInterface runInt){
        final RunInterface r = runInt;
        final View v = view;
        ViewTreeObserver viewTreeObserver = v.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    r.run();
                }
            });
        }
    }
}
