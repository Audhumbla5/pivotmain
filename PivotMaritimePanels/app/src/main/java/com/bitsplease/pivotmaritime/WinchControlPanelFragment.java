package com.bitsplease.pivotmaritime;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;


public class WinchControlPanelFragment extends MainPanelFragment {
    //private final String name = "WINCH CONTROL";
    //private MainActivity ma;
    //private State super.getState();
    private WinchControlSlider winch;
    private WinchHorizontalSlider horWinch;
    private RelativeLayout winchBackground;
    private RelativeLayout otherWinchBackground;
    private double progress = 0;

    private RadioButton[] speeds;
    private RadioButton speed1;
    private RadioButton speed2;
    private RadioButton speed3;
    private RadioButton speed4;
    private RadioButton aut;
    private RadioButton man;
    private ToggleButton emergRelease;
    private ToggleButton brake;
    private RadioButton constRpm;
    private RadioButton comb;
    private ToggleButton sternView;

    boolean initialised = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     * Inflates the view of the fragment
     *
     * @param inflater turns the corresponding XML into view objects
     * @param container Viewgroup that contains all views
     * @param savedInstanceState Previous state of activity
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_panel_winchcontrol, container, false);
    }

    public static WinchControlPanelFragment newInstance() {
        WinchControlPanelFragment fragment = new WinchControlPanelFragment();
        return fragment;
    }

    /**
     * Used for assigning the variables from the corresponding XML document.
     * Apart from speed buttons which is done onStart() method
     * Also sets the sliders sizes and boundaries. This method is called
     * whenever this panel is bought back into users view
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //wait for the activity to start then create the components
        winchBackground = (RelativeLayout)getView().findViewById(R.id.winchcontrolslider);
        winch = new WinchControlSlider(getActivity(), winchBackground, R.drawable.telegraphred);
        winch.registerObserver(this);
        otherWinchBackground = (RelativeLayout)getView().findViewById(R.id.otherwinch);
        horWinch = new WinchHorizontalSlider(getActivity(), otherWinchBackground, R.drawable.helmbutton);
        horWinch.registerObserver(this);

        //wait for the winchBackground view to finish inflating then...
        ViewTreeObserverUtility.SetUpViewTreeObserver(winchBackground, new RunInterface() {
            @Override
            public void run() {
                    winch.setBackgroundWidth(winchBackground.getWidth());
                    winch.setBackgroundHeight(winchBackground.getHeight());
                    winch.setTopBoundary(0.15);
                    winch.setBottomBoundary(0.85);
                    winch.setStickSize(winchBackground.getWidth()/3,winchBackground.getWidth()/4);
                    winch.center();
                    winchBackground.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View arg0, MotionEvent arg1) {
                            winch.drawStick(arg1);
                            return true;
                        }
                    });
            }

        });





        ViewTreeObserver viewTreeObserver2 = otherWinchBackground.getViewTreeObserver();
        if (viewTreeObserver2.isAlive()) {
            viewTreeObserver2.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    otherWinchBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    //System.out.println("w" + winchBackground.getWidth() + "h" + winchBackground.getHeight());
                    horWinch.passXY(otherWinchBackground.getWidth(), otherWinchBackground.getHeight());
                    horWinch.setLeftBoundary(0.15);
                    horWinch.setRightBoundary(0.85);
                    horWinch.setStickSize(otherWinchBackground.getHeight()/3,otherWinchBackground.getHeight()/4);
                    horWinch.center();

                    otherWinchBackground.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View arg0, MotionEvent arg1) {
                            horWinch.drawStick(arg1);
                            return true;
                        }
                    });
                }

            });
        }
    }

    /**
     * Makes the panel its initial state
     * That is speed1 button on. Unless already initialised. If initialising
     * for first time will set a boolean to let class no to not initialise again
     * Also assigns speed buttons from there XML document
     */
    @Override
    public void onStart() {
        super.onStart();
        ViewTreeObserverUtility.SetUpViewTreeObserver(getView(), new RunInterface() {
            @Override
            public void run() {
                speeds = new RadioButton[4];
                speeds[0] = (RadioButton) getView().findViewById(R.id.winchcontrol_yellow_speed1);
                speeds[1] = (RadioButton) getView().findViewById(R.id.winchcontrol_yellow_speed2);
                speeds[2] = (RadioButton) getView().findViewById(R.id.winchcontrol_yellow_speed3);
                speeds[3] = (RadioButton) getView().findViewById(R.id.winchcontrol_yellow_speed4);

                man = (RadioButton) getView().findViewById(R.id.winchcontrol_yellow_man);
                comb = (RadioButton) getView().findViewById(R.id.winchcontrol_yellow_comb);

                if(initialised==false){
                    if(!speeds[1].isChecked()){
                        speeds[1].toggle();
                    }
                    if(!man.isChecked()){
                        man.toggle();
                    }
                    if(!comb.isChecked()){
                        comb.toggle();
                    }
                    initialised=true;
                }

            }
        });
    }

    //Not currently in use
    public interface WinchControlListener{
        RadioGroup getWinchControlRadioGroupByStringId (String id);
    }
    //Not currently in use
    public RadioGroup getRadioGroup (String id){
        RadioGroup rg = (RadioGroup)getView().findViewById(getResources().getIdentifier(id, "id", "com.bitsplease.pivotmaritime"));
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                System.out.println("Thrusters Motors RadioGroup Changed\n");
            }
        });
        return rg;
    }



    /**
     * The winch slider will call this when it changes progress.
     * This method will then use notifyObserver to let the activity
     * that houses it to no of the change
     *
     * @param progress New value of slider
     */
    public void winchControlSliderUpdate(double progress){
        this.progress = progress;
        notifyObserver1();
    }

    /**
     * The horizontal winch slider will call this when it changes progress.
     * This method will then use notifyObserver to let the activity
     * that houses it to no of the change
     *
     * @param progress New value of slider
     */
    public void winchHorizontalUpdate(double progress){
        this.progress = progress;
        notifyObserver2();
    }


    /**
     * Notifies the main that progress of the winch slider has changes
     *
     */
    public void notifyObserver1() {
        super.getMainActivity().winchControlSliderUpdate(progress);
    }


    /**
     * Notifies the main that progress of the horizontal winch slider has changes
     *
     */
    public void notifyObserver2() {
        super.getMainActivity().winchHorizontalUpdate(progress);
    }

    /**
     * Sends the signal of the speed 1 button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_speed1(View v) {
        super.getState().sendMessage("winchcontrol_yellow_speed1");
    }
    /**
     * Sends the signal of the speed 2 button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_speed2(View v) {
        super.getState().sendMessage("winchcontrol_yellow_speed2");
    }
    /**
     * Sends the signal of the speed 3 button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_speed3(View v) {
        super.getState().sendMessage("winchcontrol_yellow_speed3");
    }
    /**
     * Sends the signal of the speed 4 button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_speed4(View v) {
        super.getState().sendMessage("winchcontrol_yellow_speed4");
    }
    /**
     * Sends the signal of the emergency release button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_Emergrelease(View v) {
        super.getState().sendMessage("winchcontrol_yellow_emergrelease");
    }
    /**
     * Sends the signal of the brake button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_Brake(View v) {
        super.getState().sendMessage("winchcontrol_yellow_brake");
    }
    /**
     * Sends the signal of the auto button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_Aut(View v) {
        super.getState().sendMessage("winchcontrol_yellow_aut");
    }
    /**
     * Sends the signal of the manual button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_Man(View v) {
        super.getState().sendMessage("winchcontrol_yellow_man");
    }
    /**
     * Sends the signal of the const rpm button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_Constrpm(View v) {
        super.getState().sendMessage("winchcontrol_yellow_constrpm");
    }
    /**
     * Sends the signal of the comb button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_Comb(View v) {
        super.getState().sendMessage("winchcontrol_yellow_comb");
    }
    /**
     * Sends the signal of the sternview button to server
     *
     * @param v this view must be passed in
     */
    public void pressWinchControl_Yellow_Sternview(View v) {
        super.getState().sendMessage("winchcontrol_yellow_sternview");
    }
}
