package com.bitsplease.pivotmaritime;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

/**
 * Created by stewartburnell on 30/08/16.
 */

public class TitleBarFragment extends Fragment {

    private String title = "";
    private String nPanelLeft = "";
    private String nPanelRight = "";
    private TextView nextPanelLeft;
    private TextView nextPanelRight;
    private TextView titleBarTitle;
    private boolean disableArrows = false;

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_titlebar, container, false);
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /**
     * At this stage in the Fragments lifecycle all of it's XML has been inflated and so can
     * use the getView() methods to use those Views without getting NullPointerExceptions.
     * This is where the TitleBar Fragments title and next panel arrow text is set based on the
     * the instances variables. If disableArrows has been set to true then the Views that house the
     * left and right arrows are found that their children Views are removed and the View becomes unclickable.
     */
    @Override
    public void onResume() {
        super.onResume();
        titleBarTitle = (TextView)getView().findViewById(R.id.titleBarTitle);
        titleBarTitle.setText(title);

        nextPanelLeft = (TextView)getView().findViewById(R.id.titleBarNextLeft);
        nextPanelLeft.setText(nPanelLeft);

        nextPanelRight = (TextView)getView().findViewById(R.id.titleBarNextRight);
        nextPanelRight.setText(nPanelRight);

        if(disableArrows) {
            final LinearLayout left = (LinearLayout) getActivity().findViewById(R.id.left);
            final LinearLayout right = (LinearLayout) getActivity().findViewById(R.id.right);
            left.removeAllViews();
            left.setClickable(false);
            right.removeAllViews();
            right.setClickable(false);
        }
    }

    /**
     * This is used to set the text of the title bar to the name of the panel.
     * It will then add the fragment to a LinkedList which contains all title bars
     *
     * @param title The name which will appear at the top of the panel
     * @param lltbf A list which contains all title bar fragments
     * @return The titlebar fragment
     */
    public static TitleBarFragment createTitleBarFragment(String title, LinkedList<TitleBarFragment> lltbf) {
        TitleBarFragment tbf = new TitleBarFragment();
        tbf.setTitle(title);
        lltbf.add(tbf);
        return tbf;
    }

    /**
     * Sets the arrows text which displays which panel is to the left and right.
     * The method takes a list and will then set the text of the left and right arrows depending on
     * where it is on the list by iterating through.
     *
     * @param lltbf The linkedlist that contains all titlebar fragments
     */
    public static void setLeftRightLabels(LinkedList<TitleBarFragment> lltbf){
        if(lltbf.size() > 1) {
            for (int i = 0; i < lltbf.size(); i++) {
                if (i == 0) { // if fragment is first in the list
                    lltbf.get(i).setNextPanelLeft(lltbf.get(lltbf.size() - 1).getTitle());
                    lltbf.get(i).setNextPanelRight(lltbf.get(i + 1).getTitle());
                } else if (i == lltbf.size() - 1) { // if fragment is last in the list
                    lltbf.get(i).setNextPanelLeft(lltbf.get(i - 1).getTitle());
                    lltbf.get(i).setNextPanelRight(lltbf.get(0).getTitle());
                } else { // set left text to panel that is i-1 in the list, and set right to panel that is i+1 in the list
                    lltbf.get(i).setNextPanelLeft(lltbf.get(i - 1).getTitle());
                    lltbf.get(i).setNextPanelRight(lltbf.get(i + 1).getTitle());
                }
            }
        }
    }

    /**
     * Will set disableArrows true for the TitleBarFragment passed in so that it can be queried when
     * the Fragment has loaded (during the Fragment's onResume() method).
     *
     * @param tbf The TitleBarFragment that we want to disable the next panel arrow buttons for.
     */
    public static void deleteArrows(TitleBarFragment tbf){
        tbf.setDisableArrows(true);
    }


    /**
     * Sets the variable title to the name of the panel
     * The variable is then used in the createTitleBarFragment method
     *
     * @param title The name we would like to call our panel
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Sets the variable disableArrows to be true or false.
     * If true then disable the "next panel" arrow buttons so they are not clickable.
     *
     * @param disable Whether or not to disable to the arrow buttons in the Title Bar Fragment.
     */
    public void setDisableArrows(final Boolean disable) {
        this.disableArrows = disable;
    }

    /**
     * Used to check whether the next panel arrows clickable function has been disabled.
     *
     * @return Boolean which describes where the next panel arrows clickable function has been disabled.
     */
    public boolean getDisableArrows(){
        return disableArrows;
    }

    /**
     * Used to get the name of the title of the titlebar
     *
     * @return String which is the name of the panel
     */
    public String getTitle(){
        return title;
    }

    /**
     * Sets a String variable to the name of the panel which will be to the left of this fragment
     *
     * @param nextPanelLeft name of panel to the left
     */
    public void setNextPanelLeft(String nextPanelLeft) {
        this.nPanelLeft = nextPanelLeft;
    }

    /**
     * Sets a String variable to the name of the panel which will be to the right of this fragment
     *
     * @param nextPanelRight name of panel to the right
     */
    public void setNextPanelRight(String nextPanelRight) {
        this.nPanelRight = nextPanelRight;
    }
}
