package com.bitsplease.pivotmaritime;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;


import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Pattern;


public class MainActivity extends Activity implements
        GestureDetector.OnGestureListener
{
    
    int panelNum;
    int numberOfPanels;

    //  Swipe between panels
    private GestureDetector gestureDetector;

    //  Full screen functionality
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHideRunnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            mContentView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            );
        }
    };
    private final Runnable mShowRunnable = new Runnable() {
        @Override
        public void run() {
            mContentView.setVisibility(View.GONE);
        }
    };


    private LinkedList<TitleBarFragment> titleBarFragments;

    //  Fragment variables for the dynamic main panels
    private ThrustersPanelFragment thrusterspanel;
    private WinchControlPanelFragment winchcontrolpanel;
    private PointOfViewCameraPanelFragment pointofviewcamerapanel;
    /**
     *  You can add a new main panel fragment variable here.
     */
    private LinkedList<MainPanelFragment> mainPanelFragments;
    //

    //  Fragment variables for the static/fixed fragments
    private TelegraphFragment telegraph;
    private HelmFragment helm;

    private State state;

    //  FragmentTransaction ft1 takes care of the title bar fragments changing
    private FragmentTransaction ft1;
    //  FragmentTransaction ft2 takes care of the main panel fragments changing
    private FragmentTransaction ft2;

    private MediaPlayer mp;

    private TextView helmProgressView;
    private TextView telegraphLeftProgressView;
    private TextView telegraphRightProgressView;

    //  I use these to determine when to play click sound.
    int leftTelegraphSliderInt = 0;
    int rightTelegraphSliderInt = 0;
    int helmSliderInt = 0;
    int winchHorInt = 0;
    //  set when a click is generated, eg every time the progress value goes up or down 5
    int clickEvery = 10;

    // this boolean is used by POV camera panel to determine if App has just been launched
    boolean pointOfViewInitilised = false;


    /**
     * This creates the main activity.
     * It will add in all fragments that have been chosen by admin, or if skipping that step
     * will then add all three panels. All fragments are added into lists so that we can
     * use these to change between screens. It then initialises one to the users view
     *
     * @param savedInstanceState Previous state information of activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  These must be first two methods called
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //  Swipe between panels
        gestureDetector = new GestureDetector(this, this);
        //  Full screen functionality
        mContentView = findViewById(R.id.main);
        hide();


        //  Create all main panel fragments and add them to the linked list
        mainPanelFragments = new LinkedList<>();

        boolean ignoreAdminCheckboxes = false;


        if(getIntent().getExtras() == null){
            ignoreAdminCheckboxes = true;
        }

        if (ignoreAdminCheckboxes || (getIntent().getExtras().getBoolean("thrusters"))){
            thrusterspanel = new ThrustersPanelFragment();
            thrusterspanel.setName("THRUSTERS");
            mainPanelFragments.add(thrusterspanel);
        }
        if (ignoreAdminCheckboxes || (getIntent().getExtras().getBoolean("winchcontrol"))) {
            winchcontrolpanel = new WinchControlPanelFragment();
            winchcontrolpanel.setName("WINCH CONTROL");
            mainPanelFragments.add(winchcontrolpanel);
        }
        if (ignoreAdminCheckboxes || (getIntent().getExtras().getBoolean("pointofview"))) {
            pointofviewcamerapanel = new PointOfViewCameraPanelFragment();
            pointofviewcamerapanel.setName("POINT OF VIEW / CAMERA");
            mainPanelFragments.add(pointofviewcamerapanel);
        }

        /**
         *  Create a new main panel fragment here, set its name and add it to the mainPanelFragments linked list.
         */

        //  This creates a linked list of Title Bar Fragments and for every MainPanelFragment
        //  it will add a new Title
        titleBarFragments = new LinkedList<>();
        for(MainPanelFragment mpf: mainPanelFragments){
            TitleBarFragment.createTitleBarFragment(mpf.getName(), titleBarFragments);
        }
        //  Inside TitleBarFragment class is a method that cycles through the Title Bar Fragments
        //  linked list and sets the next fragment text for left and right based on which fragments are next
        //  to it in the linked list
        if(mainPanelFragments.size()>1) {
            TitleBarFragment.setLeftRightLabels(titleBarFragments);
        }
        else if (mainPanelFragments.size()==1){

            TitleBarFragment.deleteArrows(titleBarFragments.get(0));

        }
        //  Determined by how many main panel frags there are in the linked list
        numberOfPanels = mainPanelFragments.size();
        //  Initialise panelNum so first panel added to the linked list is loaded on the screen first
        panelNum = 0;

        //  Add the default Fragments
        ft1 = getFragmentManager().beginTransaction();
        ft2 = getFragmentManager().beginTransaction();
            //Title
            ft1.add(R.id.titlebar, titleBarFragments.get(panelNum)).commit();
            //Panel
            ft2.add(R.id.panel, mainPanelFragments.get(panelNum)).commit();

        //Helm and Telegraph Fragments are added by XML, not dynamically added
        telegraph = (TelegraphFragment)getFragmentManager().findFragmentById(R.id.telegraphFrag);
        helm = (HelmFragment)getFragmentManager().findFragmentById(R.id.helmFrag);
        helmProgressView = (TextView) findViewById(R.id.helmProgress);
        telegraphLeftProgressView = (TextView) findViewById(R.id.telegraphLeftProgress);
        telegraphRightProgressView = (TextView) findViewById(R.id.telegraphRightProgress);


        //  Create a State object where we can store the state of our panels as well as communicate to
        //  the simulator.
        state = new State();
        state.start();

        //  Every MainPanelFragment that was added to the linked list during onCreate now has access
        //  to the state object as well as a reference to this MainActivity in line with the
        //  Observer/Observable programming pattern.
        for(MainPanelFragment mpf: mainPanelFragments){
            mpf.registerObserver(this);
            mpf.passInState(state);
        }
    }

    /**
     * Will attach a sound file to a variable that we will use for all clicking sounds
     */
    @Override
    protected void onStart() {
        super.onStart();
        mp = MediaPlayer.create(this, R.raw.click);
        //testState
    }

    /**
     * When app is exited this will be called and send a signal to the server
     * This way the server can shutdown and save to text when it gets this signal
     * It also writes the state class object to file so in the future
     * whoever is using can implement a read object, to read from file
     * as that was not completed
     */
    @Override
    protected void onStop() {
        super.onStop();
        try {
            String fileName = "state";
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            state.writeObject(fos);
            state.sendMessage("closefile");

        }
        catch (IOException e)
        {
            System.out.println(getFilesDir());
            System.out.println("ERROR ERROR ERROR: " + e.getMessage());
        }
    }

    /**
     * This method is called when the main is first created.
     * Fragments and telegraph and helm sliders that change progress use
     * this method to register that they will be reporting there changes of progress to the main
     *
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        //  Pass a reference of this MainActivity to the Telegraph and Helm Fragments.
        //  This means that they can call methods that belong to the MainActivity.
        telegraph.registerObserver(this);
        helm.registerObserver(this);

        //  Every MainPanelFragment that was added to the linked list during onCreate now has access
        //  to the state object as well as a reference to this MainActivity in line with the
        //  Observer/Observable programming pattern.
        for(MainPanelFragment mpf: mainPanelFragments){
            mpf.registerObserver(this);
            mpf.passInState(state);
        }

        //  Hide the system UI.
        hide();
    }


    //  Override the Activity method onTouchEvent so that all observed MotionEvents get passed to
    //  the GestureDetector object you declared above.
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    //  The following methods must be implemented because "implements GestureDetector.OnGestureListener".
    //  Only used onFling though.
    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    /**
     * Launches admin panel alert box.
     * WHen user does a long hold on screen
     *
     * @param e Users touch event
     */
    @Override
    public void onLongPress(MotionEvent e) {
        adminPanelAlert();
    }

    /**
     * Launches the alert for the admin panel
     * User can then choose whether to go into admin panel login or stay where they are
     *
     */
    public void adminPanelAlert(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(i);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        hide();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to go to the Admin panel?")
                .setNegativeButton("No", dialogClickListener).setPositiveButton("Yes", dialogClickListener).show();
        hide();
    }

    // Currently disabled but can be used for swiping gestures
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        // e1 is where the finger starts.
        // e2 is where the finger ends.
        // Use getX() and getY() methods on e1 and e2.
        if( e1.getX() < e2.getX() && (e2.getX()-e1.getX() > 60) ) //SWIPE LEFT
        {
            // If finger moved from left to right && finger moved a distance greater than 100.
            // "Swipe left" as in push current scene to right to get to the left scene.

            // swipeLeft();
        }
        else if( e1.getX() > e2.getX() && (e1.getX()-e2.getX() > 60) ) //SWIPE RIGHT
        {
            // If finger moved from right to left && finger moved a distance greater than 100.
            // "Swipe right" as in push current scene to left to get to the right scene.

            // swipeRight();
        }
        return true;
    }

    /**
     * Calls swipe left method to change fragments to the panel on the left
     *
     * @param v This view must be passed
     */
    public void clickLeft(View v){
        swipeLeft();

    }

    /**
     * Calls swipe right method to change fragments to the panel on the right
     *
     * @param v This view must be passed
     */
    public void clickRight(View v){
        swipeRight();
    }

    /**
     * Gets the panel on the left and changes the fragment of panel and title to those that
     * are on the left and brings into users view
     *
     */
    public void swipeLeft()
    {
        panelNum--;
        if (panelNum <= -1)
        {
            panelNum = numberOfPanels-1;
        }

        FragmentTransaction ft3 = getFragmentManager().beginTransaction();
        FragmentTransaction ft4 = getFragmentManager().beginTransaction();

        ft3.replace(R.id.titlebar, titleBarFragments.get(panelNum)).commit();
        ft4.replace(R.id.panel, mainPanelFragments.get(panelNum)).commit();
        // Must force transactions to take place now before moving on
        getFragmentManager().executePendingTransactions();
    }

    /**
     * Gets the panel on the right and changes the fragment of panel and title to those that
     * are on the right and brings into users view
     *
     */
    public void swipeRight()
    {
        panelNum++;
        if (panelNum >= numberOfPanels)
        {
            panelNum = 0;
        }

        FragmentTransaction ft3 = getFragmentManager().beginTransaction();
        FragmentTransaction ft4 = getFragmentManager().beginTransaction();

        ft3.replace(R.id.titlebar, titleBarFragments.get(panelNum)).commit();
        ft4.replace(R.id.panel, mainPanelFragments.get(panelNum)).commit();
        // Must force transactions to take place now before moving on
        getFragmentManager().executePendingTransactions();
    }


    /**
     * Hides system UI
     */
    private void hide() {
        mHideHandler.removeCallbacks(mShowRunnable);
        mHideHandler.postDelayed(mHideRunnable, 0);
    }

    /**
     * Used to play the clicking sound when winch slider is moved
     *
     * @param progress Value of the slider
     */
    public void winchControlSliderUpdate(double progress){
        // the progress values that comes out range from 100 to -100 so remember to use the absolute value of progress
        // use the progress to drive the vol
        progress = Math.abs(progress);
        mp.setVolume((float)progress/100, (float)progress/100);

        mp.start();

    }

    /**
     * Used to play the sound for the horizontal slider on winch control panel
     * Clicks at certain increments decided by the clickEvery variable
     *
     * @param progress value of the slider
     */
    public void winchHorizontalUpdate(double progress){
        //  cast progress as int so mod function can be applied to it
        int prog = (int)progress;
        //System.out.println(progress);
        if(prog%clickEvery==0 && prog != winchHorInt) {
            winchHorInt = prog;
            mp.setVolume(1, 1);
            mp.start();
        }
    }

    /**
     * Used to play the sound for the helm slider
     * Clicks at certain increments decided by the clickEvery variable
     *
     * @param progress value of the slider
     */
    public void helmSliderUpdate(double progress){
        //  cast progress as int so mod function can be applied to it
        int prog = (int)progress;
        helmProgressView.setText(""+prog);
        if(prog%clickEvery==0 && prog != helmSliderInt) {
            helmSliderInt = prog;
            mp.setVolume(1, 1);
            mp.start();
            state.sendMessage("helmslider."+Integer.toString(prog));
            //System.out.println(progress);
        }
    }

    /**
     * Used to play the sound for the left telegraph slider
     * Clicks at certain increments decided by the clickEvery variable
     *
     * @param progress value of the slider
     */
    public void leftTelegraphSliderUpdate(double progress){
        //  cast progress as int so mod function can be applied to it
        int prog = (int)progress;
        telegraphLeftProgressView.setText(""+prog);
        if(prog%clickEvery==0 && prog != leftTelegraphSliderInt) {
            leftTelegraphSliderInt = prog;
            mp.setVolume(1, 1);
            mp.start();
            state.sendMessage("lefttelegraphslider."+Integer.toString(prog));
            //System.out.println(progress);
        }
    }

    /**
     * Used to play the sound for the right telegraph slider
     * Clicks at certain increments decided by the clickEvery variable
     *
     * @param progress value of the slider
     */
    public void rightTelegraphSliderUpdate(double progress){
        //  cast progress as int so mod function can be applied to it
        int prog = (int)progress;
        telegraphRightProgressView.setText("" + prog);
        if(prog%clickEvery==0 && prog != rightTelegraphSliderInt) {
            rightTelegraphSliderInt = prog;
            mp.setVolume(1, 1);
            mp.start();
            state.sendMessage("righttelegraphslider."+Integer.toString(prog));
            //System.out.println(progress);
        }
    }

    /**
     * Used to play sound for the joystick
     *
     */
    public void joyStickUpdate(){
        mp.setVolume((float)10/100, (float)10/100);
        mp.start();

    }


    //  These three methods are called when you press the reset symbol and numbers on the tablet

    /**
     * When reset button on the helm is pressed this resets the slider to Zero
     *
     * @param v This view must be passed
     */
    public void resetHelmToZero(View v)
    {
        helm.center();
    }
    /**
     * When reset button on the left telegraph is pressed this resets the slider to Zero
     *
     * @param v This view must be passed
     */
    public void resetTelegraphLeftToZero(View v)
    {
        telegraph.centerLeft();
    }
    /**
     * When reset button on the right telegraph is pressed this resets the slider to Zero
     *
     * @param v This view must be passed
     */
    public void resetTelegraphRightToZero(View v)
    {
        telegraph.centerRight();
    }
    //

    //call method directly from xml

    public void buttonActivated(View v) {
        thrusterspanel.genericButtonPress((ToggleButton) findViewById(v.getId()));
    }

    /**
     * Turns on/off Points of view cameras North west.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewNorthWest(View v) {
        pointofviewcamerapanel.pressPointOfViewNorthWest();
    }
    /**
     * Turns on/off Points of view cameras North.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewNorth(View v) {
        pointofviewcamerapanel.pressPointOfViewNorth();

    }
    /**
     * Turns on/off Points of view cameras North east.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewNorthEast(View v) {
        pointofviewcamerapanel.pressPointOfViewNorthEast();
    }
    /**
     * Turns on/off Points of view cameras East.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewEast(View v) {

        pointofviewcamerapanel.pressPointOfViewEast();
    }
    /**
     * Turns on/off Points of view cameras South East.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewSouthEast(View v) {
        pointofviewcamerapanel.pressPointOfViewSouthEast();
    }
    /**
     * Turns on/off Points of view cameras South.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewSouth(View v)
    {
        pointofviewcamerapanel.pressPointOfViewSouth();
    }
    /**
     * Turns on/off Points of view cameras South West.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewSouthWest(View v) {
        pointofviewcamerapanel.pressPointOfViewSouthWest();
    }
    /**
     * Turns on/off Points of view cameras West.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewWest(View v)
    {
        pointofviewcamerapanel.pressPointOfViewWest();
    }
    /**
     * Turns on/off Points of view cameras Port wing.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewRedPortWing(View v) {
        pointofviewcamerapanel.pressPointOfViewRedPortWing();
    }
    /**
     * Turns on/off Points of view cameras Yellow brdg.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewYellowBrdg(View v) {
        pointofviewcamerapanel.pressPointOfViewYellowBrdg();
    }
    /**
     * Turns on/off Points of view cameras Stbd Wing.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewGreenStbdWing(View v) {
    pointofviewcamerapanel.pressPointOfViewGreenStbdWing();
    }
    /**
     * Turns on/off Points of view cameras Yellow pitch.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewCameraYellowPitch(View v) {
        pointofviewcamerapanel.pressPointOfViewCameraYellowPitch();
    }
    /**
     * Turns on/off Points of view cameras Reset.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressPointOfViewCameraYellowReset(View v) {
        pointofviewcamerapanel.pressPointOfViewCameraYellowReset();
    }
    /**
     * Turns on/off Winch Control Speed 1.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_speed1(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_speed1(v);
    }
    /**
     * Turns on/off Winch Control Speed 2.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_speed2(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_speed2(v);

    }
    /**
     * Turns on/off Winch Control Speed 3.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_speed3(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_speed3(v);
    }
    /**
     * Turns on/off Winch Control Speed 4.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_speed4(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_speed4(v);
    }
    /**
     * Turns on/off Winch Control Emergency Release.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_Emergrelease(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_Emergrelease(v);
    }
    /**
     * Turns on/off Winch Control Brake.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_Brake(View v){
        winchcontrolpanel.pressWinchControl_Yellow_Brake(v);
    }
    /**
     * Turns on/off Winch Control Auto.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_Aut(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_Aut(v);
    }
    /**
     * Turns on/off Winch Control Speed Manual.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_Man(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_Man(v);
    }
    /**
     * Turns on/off Winch Control Const RPM.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_Constrpm(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_Constrpm(v);
    }
    /**
     * Turns on/off Winch Control Comb.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_Comb(View v) {
        winchcontrolpanel.pressWinchControl_Yellow_Comb(v);
    }
    /**
     * Turns on/off Winch Control Stern View.
     * Also signal will be sent to server
     *
     * @param v This view must be passed in
     */
    public void pressWinchControl_Yellow_Sternview(View v) {

        winchcontrolpanel.pressWinchControl_Yellow_Sternview(v);
    }



    // Getter and setter to be used from the POV Fragment for use in when the fragment is created
    // to determine if the user has been to this fragment yet

    /**
     * Used to determine if the point of view camera panel has already been initialised.
     * This is so that we dont always go back to initial state
     *
     * @return Boolean whether or not initialised
     */
    public boolean getPointOfViewInitilised(){
        return pointOfViewInitilised;
    }

    /**
     * Sets the variable pointOfViewInitilised to true so that we only initialise the camera panel once
     *
     *
     */
    public void initialisePointOfViewBool(){
        pointOfViewInitilised = true;
    }
}



