package com.bitsplease.pivotmaritime;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by stewartburnell on 4/05/16.
 *
 * This class facilitates communication with the computer running the Pivot Maritime Simulator.
 * It also maintains the state of buttons and sliders of each panel in a central place facilitating loading
 * and saving between State object and different panels.
 *
 */
public class State extends Thread implements Serializable {

    /**
     * SET THIS TO TRUE IF YOU WANT TO COMMUNICATE WITH SIMULATOR COMPUTER
     */
        final boolean BLUETOOTH_ENABLED = false;
    /**
     *
     */

    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    /**
     * Set your UUID and set the receiving computer's MAC address.
     */
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = "00:1A:7D:DA:71:14";
    //Lee's device 60:57:18:2F:79:A6
    //Stew's desktop 00:15:83:E4:57:F8
    //Jacob's desktop 5C:F3:70:61:0D:BE

    // Thrusters Panel
    private boolean thrusters_bow_red_full;
    private boolean thrusters_bow_red_half;
    private boolean thrusters_bow_yellow_stop;
    private boolean thrusters_bow_green_half;
    private boolean thrusters_bow_green_full;
    private boolean thrusters_motors_red_stop;
    private boolean thrusters_motors_green_start;
    private boolean thrusters_stern_red_full;
    private boolean thrusters_stern_red_half;
    private boolean thrusters_stern_yellow_stop;
    private boolean thrusters_stern_green_half;
    private boolean thrusters_stern_green_full;
    // Point Of View Camera Panel
    private boolean pointofview_northwest;
    private boolean pointofview_north;
    private boolean pointofview_northeast;
    private boolean pointofview_east;
    private boolean pointofview_southeast;
    private boolean pointofview_south;
    private boolean pointofview_southwest;
    private boolean pointofview_west;
    private boolean pointofview_red_portwing;
    private boolean pointofview_yellow_brdg;
    private boolean pointofview_green_stbdwing;
    private boolean camera_yellow_pitch;
    private boolean camera_yellow_reset;
    private int joystick_x_axis;
    private int joystick_y_axis;
    // Winch Control
    private boolean winchcontrol_yellow_speed1;
    private boolean winchcontrol_yellow_speed2;
    private boolean winchcontrol_yellow_speed3;
    private boolean winchcontrol_yellow_speed4;
    private boolean winchcontrol_yellow_emergrelease;
    private boolean winchcontrol_yellow_brake;
    private boolean winchcontrol_yellow_aut;
    private boolean winchcontrol_yellow_man;
    private boolean winchcontrol_yellow_constrpm;
    private boolean winchcontrol_yellow_comb;
    private boolean winchcontrol_yellow_sternview;

    private int winchcontrol_rotarywinchclutch;
    private int winchcontrol_winchslider;
    // Helm
    private int helmslider;
    // Telegraph
    private int telegraphL;
    private int telegraphR;


    /**
     * Constructor for State includes a variable for every button and slider etc on all the panels.
     * When State object is created they are all set to false or 0 depending on variable type.
     * Using these variables the State object could be written to a text file for extra redundancy and in the
     * case that something happens to the State object, can be read from.
     * Reading and writing to text file is only an option and hasn't been implemented.
     */
    public State() {
        // Set initialised states
        this.thrusters_bow_red_full = false;
        this.thrusters_bow_red_half = false;
        this.thrusters_bow_yellow_stop = true;
        this.thrusters_bow_green_half = false;
        this.thrusters_bow_green_full = false;
        this.thrusters_motors_red_stop = true;
        this.thrusters_motors_green_start = false;
        this.thrusters_stern_red_full = false;
        this.thrusters_stern_red_half = false;
        this.thrusters_stern_yellow_stop = true;
        this.thrusters_stern_green_half = false;
        this.thrusters_stern_green_full = false;
        // Point Of View Camera Panel
        this.pointofview_northwest = false;
        this.pointofview_north = false;
        this.pointofview_northeast = false;
        this.pointofview_east = false;
        this.pointofview_southeast = false;
        this.pointofview_south = false;
        this.pointofview_southwest = false;
        this.pointofview_west = false;
        this.pointofview_red_portwing = false;
        this.pointofview_yellow_brdg = false;
        this.pointofview_green_stbdwing = false;
        this.camera_yellow_pitch = false;
        this.camera_yellow_reset = false;
        // JoyStick --> ID: camera_joystick
        this.joystick_x_axis = 0;
        this.joystick_y_axis = 0;
        // Winch Control
        this.winchcontrol_yellow_speed1 = false;
        this.winchcontrol_yellow_speed2 = false;
        this.winchcontrol_yellow_speed3 = false;
        this.winchcontrol_yellow_speed4 = false;
        this.winchcontrol_yellow_emergrelease = false;
        this.winchcontrol_yellow_brake = false;
        this.winchcontrol_yellow_aut = false;
        this.winchcontrol_yellow_man = false;
        this.winchcontrol_yellow_constrpm = false;
        this.winchcontrol_yellow_comb = false;
        this.winchcontrol_yellow_sternview = false;
        // Both are seek bars
        this.winchcontrol_rotarywinchclutch = 0;
        this.winchcontrol_winchslider = 0;
        // Helm
        this.helmslider = 0;
        // Telegraph
        this.telegraphL = 0;
        this.telegraphR = 0;

    }

    /**
     * This method only executes if the BLUETOOTH_ENABLED boolean has been set to true.
     * This is a way of controlling whether to have the app trying to communicate to the simulator
     * during development. If the code is run and the device's Bluetooth isn't turned on then errors will occur.
     * All of the Bluetooth communication is initialised here so that messages can be sent.
     */
    public void run(){
        if(BLUETOOTH_ENABLED) {
            btAdapter = BluetoothAdapter.getDefaultAdapter();
            //checkBtState();
            BluetoothDevice device = btAdapter.getRemoteDevice(address);
            try {
                btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                System.out.println("Error creating socket");
            }
            btAdapter.cancelDiscovery();
            try {
                btSocket.connect();
            } catch (IOException e) {
                try {
                    btSocket.close();
                } catch (IOException e2) {
                    System.out.println("Error creating socket");
                }
            }
            try {
                outStream = btSocket.getOutputStream();
            } catch (IOException e) {
                System.out.println("Outstream failed");
            }
        }
    }

    /**
     * This method only executes if the BLUETOOTH_ENABLED boolean has been set to true.
     * When buttons are pressed in eg. ThrustersPanelFragment, the fragments State object will be used to call
     * this method and a String message will be passed in relating to eg. which button was pressed or what value
     * the sliders have changed to. The message received on the computer hosting the Simulator software
     * should be used to control the Simulator's controls.
     * This method is the communication part of the app.
     *
     * @param message The message you are sending across Bluetooth to the receiver.
     */
    public void sendMessage(String message){
        if(BLUETOOTH_ENABLED) {
            message += "\n";
            byte[] messageBuffer = message.getBytes();
            try {
                outStream.write(messageBuffer);
                outStream.flush();

                //btSocket.close();
                System.out.println("send complete");
            } catch (IOException e) {
                System.out.println("Message fail");
            }
        }
    }
     /*
    public void closeSocket(){
        btSocket.close();
    }
    */

    /**
     * This function will save the State of the Thrusters buttons into the State object from a Toggle
     * Button array passed in. It is called by the instance of State in the ThrustersPanelFragment's code
     * This enables the State to be stored outside of the panel in case the
     * Fragment goes out of scope while another panel is being viewed and is cleared from memory.
     * A new ThrustersPanelFragment can be instantiated and initialise its button states from the
     * State object passed in using the sendThrustersButtonStates() method in this class.
     *
     * @param tb is the ToggleButton array passed in from ThrustersPanelFragment
     */
    public void readThrusterButtonStates(ToggleButton[] tb){
        this.thrusters_bow_red_full = tb[0].isChecked();
        this.thrusters_bow_red_half = tb[1].isChecked();
        this.thrusters_bow_yellow_stop = tb[2].isChecked();
        this.thrusters_bow_green_half = tb[3].isChecked();
        this.thrusters_bow_green_full = tb[4].isChecked();
        this.thrusters_motors_red_stop = tb[5].isChecked();
        this.thrusters_motors_green_start = tb[6].isChecked();
        this.thrusters_stern_red_full = tb[7].isChecked();
        this.thrusters_stern_red_half = tb[8].isChecked();
        this.thrusters_stern_yellow_stop = tb[9].isChecked();
        this.thrusters_stern_green_half = tb[10].isChecked();
        this.thrusters_stern_green_full = tb[11].isChecked();
    }

    /**
     * This function will be called from an object of State inside ThrustersPanelFragment.
     * It will update the ToggleButton states in the ToggleButton array to what the states are
     * in this State object then return the modified ToggleButton array.
     *
     * @return ToggleButton[]
     * @param tb is the ToggleButton array passed in from ThrustersPanelFragment
     */
    public ToggleButton[] sendThrusterButtonStates(ToggleButton[] tb) {
        tb[0].setChecked(thrusters_bow_red_full);
        tb[1].setChecked(thrusters_bow_red_half);
        tb[2].setChecked(thrusters_bow_yellow_stop);
        tb[3].setChecked(thrusters_bow_green_half);
        tb[4].setChecked(thrusters_bow_green_full);
        tb[5].setChecked(thrusters_motors_red_stop);
        tb[6].setChecked(thrusters_motors_green_start);
        tb[7].setChecked(thrusters_stern_red_full);
        tb[8].setChecked(thrusters_stern_red_half);
        tb[9].setChecked(thrusters_stern_yellow_stop);
        tb[10].setChecked(thrusters_stern_green_half);
        tb[11].setChecked(thrusters_stern_green_full);
        return tb;
    }

    /**
     * Writes the state class object to file
     * This will then save the staste of the class in case the user lesaves the app
     * and needs to return to current state
     *
     * @throws IOException
     */
    public void writeObject(FileOutputStream fos) throws IOException
    {

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fos);
        objectOutputStream.writeObject(this);
        objectOutputStream.close();

    }


}
