package com.bitsplease.pivotmaritime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class HelmSlider {
    private HelmFragment hf;

    private Context mContext;
    private ViewGroup mLayout;
    private LayoutParams params;
    private int stick_width, stick_height;
    private int truex, truey;
    private DrawCanvas draw;
    private Paint paint;
    private Bitmap stick;
    private boolean touch_state = false;
    private double progress = 0;
    private double touchProgress = 0;
    private double left_boundary, right_boundary;


    public HelmSlider(Context context, ViewGroup layout, int stick_res_id) {
        mContext = context;
        stick = BitmapFactory.decodeResource(mContext.getResources(), stick_res_id);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
        draw = new DrawCanvas(mContext);
        paint = new Paint();
        mLayout = layout;
        params = mLayout.getLayoutParams();
    }

    /**
     * Used to determine if the users touch is on or very close to the slider.
     * The method will take in the position where the user touches and compare this against where the slider is
     * if it is close enough will return true and the slider can be moved, otherwise false.
     *
     * @param neededProgress position of slider
     * @param actualProgress position of touch
     * @return               whether the users touch is close enough
     */
    public boolean closeEnough(double neededProgress, double actualProgress){
        if(Math.abs(neededProgress-actualProgress)<20){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Method used to draw the slider stick onto the slider.
     * Uses the closeEnough method to determine whether the users touch is on the current position of the slider knob.
     * If it is then it will use the users touch (arg1) to determine where the user is moving and draw on the new slider knob
     * @param arg1 The users touch
     */
    public void drawStick(MotionEvent arg1) {

        //touchProgress calculates what progress your finger it at when it touches down
        touchProgress = -(((arg1.getX()-left_boundary)-0)/(right_boundary-left_boundary-0)*-200) - 100;

        if(closeEnough(progress,touchProgress)) {
            if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
                if (arg1.getX() > left_boundary && arg1.getX() < right_boundary) {
                    // progress = -(((arg1.getX()-left_boundary)-0)/(right_boundary-left_boundary-0)*-200) - 100;
                    // ^ explanation below
                    // (arg1.getX()-left_boundary)-0)/(right_boundary-left_boundary-0) is normalising the value range to 0-1
                    // normalising as in (val-min)/(max-min)
                    // this is multiplied by 200
                    // this gives range of (0 to 200) from top to bottom but we want range of (100 to -100)
                    // function f(x)=-x+100 means x = 0 is converted to 100 and x = 200 is converted to -100
                    progress = -(((arg1.getX() - left_boundary) - 0) / (right_boundary - left_boundary - 0) * -200) - 100;

                    draw.position(arg1.getX());
                    draw();

                    notifyObserver();
                    touch_state = true;
                } else if (arg1.getX() < left_boundary) {
                    progress = -(0 / (right_boundary - 0) * -200) - 100;
                    draw.position((int) left_boundary);
                    draw();

                    notifyObserver();
                    touch_state = true;
                } else if (arg1.getX() > right_boundary) {
                    progress = -(((right_boundary) - 0) / (right_boundary - 0) * -200) - 100;

                    draw.position((int) right_boundary);
                    draw();

                    notifyObserver();
                    touch_state = true;
                }
            } else if (arg1.getAction() == MotionEvent.ACTION_MOVE && touch_state) {
                if (arg1.getX() > left_boundary && arg1.getX() < right_boundary) {
                    progress = -(((arg1.getX() - left_boundary) - 0) / (right_boundary - left_boundary - 0) * -200) - 100;
                    //will set global progress to 0 if it's within a range

                    draw.position(arg1.getX());
                    draw();

                    notifyObserver();
                } else if (arg1.getX() < left_boundary) {
                    progress = -(0 / (right_boundary - 0) * -200) - 100;
                    draw.position((int) left_boundary);
                    draw();

                    notifyObserver();

                } else if (arg1.getX() > right_boundary) {
                    progress = -(((right_boundary) - 0) / (right_boundary - 0) * -200) - 100;
                    draw.position((int) right_boundary);
                    draw();

                    notifyObserver();
                }
            } else if (arg1.getAction() == MotionEvent.ACTION_UP) {
                draw();
                touch_state = false;
            }
        }
    }

    /**
     * Used to create the size of the slider knob
     * Usually used by the fragment that contains the slider.
     *
     * @param width the width of the knob
     * @param height the height of the knob
     */
    public void setStickSize(int width, int height) {
        stick = Bitmap.createScaledBitmap(stick, width, height, false);
        stick_width = stick.getWidth();
        stick_height = stick.getHeight();
    }

    /**
     * This method draw the knob onto the screen.
     * Also removes where the knob was before the movement.
     */
    private void draw() {
        try {
            mLayout.removeView(draw);
        } catch (Exception e) { }
        mLayout.addView(draw);
    }


    /**
     * Sets the total value of the X/Y axis based on size of the joystick.
     * Used by the fragment that houses the slider
     *
     * @param x The X axis
     * @param y The Y axis
     */
    public void passXY(int x, int y) {
        truex = x;
        truey = y;
    }


    /**
     * Centers the slider
     *
     */
    public void center() {
        draw.position(getTruex()/2);
        draw();
        progress = 0;
        notifyObserver();
    }

    /**
     * Sets the boundary on the left side of the slider.
     *
     * @param left determines how close/far to the left you will be
     */
    public void setLeftBoundary(double left){
        left_boundary = getTruex()*left;
    }

    /**
     * Sets the boundary on the right side of the slider.
     *
     * @param right determines how close/far to the right you will be
     */
    public void setRightBoundary(double right){
        right_boundary = getTruex()*right;
    }

    /**
     * Gives you the full value of X axis
     *
     * @return int X axis value
     */
    public int getTruex(){
        return truex;
    }

    /**
     * Gives you the full value of Y axis
     *
     * @return int Y axis value
     */
    public int getTruey(){
        return truey;
    }

    /**
     * Method used to register the slider in the fragment it belongs, the Helm Fragment
     * Now it can report progress changes
     *
     * @param hf Fragment that houses the slider
     */
    public void registerObserver(HelmFragment hf) {
        this.hf = hf;
    }

    /**
     * Notifies the fragment that houses the slider that a progress change has occurred
     *
     */
    public void notifyObserver() {hf.helmSliderUpdate(progress);
    }

    /**
     * Private class to help with the drawing of slider
     *
     */
    private class DrawCanvas extends View{
        float x, y;

        private DrawCanvas(Context mContext) {
            super(mContext);
        }

        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(stick, x, y, paint);
        }

        private void position(float pos_x) {
            x = pos_x - (stick_width / 2);
            y = getTruey()/2 - (stick_height / 2);
        }
    }

}