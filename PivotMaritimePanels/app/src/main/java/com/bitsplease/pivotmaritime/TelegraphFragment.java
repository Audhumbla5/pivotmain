package com.bitsplease.pivotmaritime;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class TelegraphFragment extends Fragment{

    MainActivity ma;
    TelegraphSlider sliderLeft;
    TelegraphSlider sliderRight;
    LinearLayout sliderLeftBackground;
    LinearLayout sliderRightBackground;
    double leftProgress = 0;
    double rightProgress = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_telegraph, container, false);
    }

    /**
     * This is where the Fragment is instantiated. The ViewTreeObserver is used to wait until the
     * backgrounds have inflated before trying to render the sliders on top.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        sliderLeftBackground = (LinearLayout)getView().findViewById(R.id.lefttelegraph);
        sliderRightBackground = (LinearLayout)getView().findViewById(R.id.righttelegraph);
        sliderLeft = new TelegraphSlider(getActivity(), sliderLeftBackground, R.drawable.telegraphwhite);
        sliderLeft.registerObserver(this);
        sliderLeft.setName("left");
        sliderRight = new TelegraphSlider(getActivity(), sliderRightBackground, R.drawable.telegraphwhite);
        sliderRight.registerObserver(this);
        sliderRight.setName("right");

        ViewTreeObserver viewTreeObserver = sliderLeftBackground.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    sliderLeftBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    sliderLeft.setBackgroundWidth(sliderLeftBackground.getWidth());
                    sliderLeft.setBackgroundHeight(sliderLeftBackground.getHeight());
                    sliderLeft.setTopBoundary(0.10);
                    sliderLeft.setBottomBoundary(0.90);
                    sliderLeft.setStickSize(sliderLeftBackground.getWidth()/2,sliderLeftBackground.getWidth()/4);
                    sliderLeft.center();

                    sliderLeftBackground.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View arg0, MotionEvent arg1) {
                            sliderLeft.drawStick(arg1);
                            return true;
                        }
                    });
                }

            });
            sliderLeft.registerObserver(this);

        }

        ViewTreeObserver viewTreeObserver2 = sliderRightBackground.getViewTreeObserver();
        if (viewTreeObserver2.isAlive()) {
            viewTreeObserver2.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    sliderRightBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    sliderRight.setBackgroundWidth(sliderRightBackground.getWidth());
                    sliderRight.setBackgroundHeight(sliderRightBackground.getHeight());
                    sliderRight.setTopBoundary(0.10);
                    sliderRight.setBottomBoundary(0.90);
                    sliderRight.setStickSize(sliderRightBackground.getWidth()/2,sliderRightBackground.getWidth()/4);
                    sliderRight.center();

                    sliderRightBackground.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View arg0, MotionEvent arg1) {
                            sliderRight.drawStick(arg1);
                            return true;
                        }
                    });
                }
            });
        }
    }



    /**
     * TelegraphSlider class calls this method when there is a change of progress
     * It then changes the value of the slider
     * This method will then call the notifyObserver method
     *
     * @param progress the new value of the slider
     * @param ts is a TelegraphSlider object that is either left or right
     */
    public void telegraphSliderUpdate(double progress, TelegraphSlider ts){
        if(ts.getName().equals("left")){
        this.leftProgress = progress;
        notifyLeftObserver();}
        else if(ts.getName().equals("right")){
            this.rightProgress = progress;
            notifyRightObserver();}
        else{
            System.out.println("Error in Telegraph Fragment \"telegraphSliderUpdate\" method");
        }
    }

    /**
     * Creates a MainActivity in the class
     *
     * @param ma main activity object
     */
    public void registerObserver(MainActivity ma) {
        this.ma = ma;
    }

    /**
     * Notifies the Main activity that a change has been made to the value of the left slider
     */
    public void notifyLeftObserver() {
        ma.leftTelegraphSliderUpdate(leftProgress);
    }

    /**
     * Notifies the Main activity that a change has been made to the value of the right slider
     */
    public void notifyRightObserver() {
        ma.rightTelegraphSliderUpdate(rightProgress);
    }

    /**
     * Centers the value of the left Telegraph slider
     */
    public void centerLeft(){
        sliderLeft.center();
    }

    /**
     * Centers the value of the right Telegraph slider
     */
    public void centerRight(){
        sliderRight.center();
    }
}