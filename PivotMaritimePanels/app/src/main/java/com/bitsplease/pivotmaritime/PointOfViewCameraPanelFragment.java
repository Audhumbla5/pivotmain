package com.bitsplease.pivotmaritime;

import android.app.Fragment;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RadioButton;
import android.widget.ToggleButton;
import android.widget.RelativeLayout;


public class PointOfViewCameraPanelFragment extends MainPanelFragment {
    //private final String name = "POINT OF VIEW / CAMERA";
    //MainActivity ma;
    private Handler handler;
    //private State state;
    private ToggleButton northWest;
    private ToggleButton north;
    private ToggleButton northEast;
    private ToggleButton east;
    private ToggleButton southEast;
    private ToggleButton south;
    private ToggleButton southWest;
    private ToggleButton west;
    private ToggleButton[] buttonArr;
    private JoyStick js;
    private RadioButton portWing;
    private RadioButton brdg;
    private RadioButton stbdWing;
    private RadioButton[] middleButtonArr;

    private ToggleButton pitch;
    private ToggleButton reset;


    /**
     * Inflates the view of the fragment
     *
     * @param inflater turns the corresponding XML into view objects
     * @param container Viewgroup that contains all views
     * @param savedInstanceState Previous state of activity
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_panel_povcamera, container, false);
        return view;
    }

    // Had to make JoyStick object in here for access to id

    /**
     * Used for assigning the variables in the class to the corresponding XML document
     * It also sets the size and look of the joystick and initialises the screen
     * to its initial state, unless it has already been done once.
     *
     * @param savedInstanceState previous state of activity
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // need to put .getView() in front of the usual findViewById
        buttonArr = new ToggleButton[8];
        middleButtonArr = new RadioButton[3];
        final RelativeLayout layout_joystick = (RelativeLayout) getView().findViewById(R.id.layout_joystick);
        js = new JoyStick(getActivity().getApplicationContext()
                , layout_joystick, R.drawable.image_button);
        js.registerObserver(this);

        ViewTreeObserver viewTreeObserver = layout_joystick.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    layout_joystick.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    //System.out.println(layout_joystick.getWidth() + " " + layout_joystick.getHeight());
                    js.setStickSize(110, 110);// Size of small red dot png
                    js.setLayoutSize(layout_joystick.getWidth(), layout_joystick.getHeight());//Yellow png size
                    js.setLayoutAlpha(500);//changes yellow transparency
                    js.setStickAlpha(1000);//changes red transparency
                    js.setOffset(70);//moves red png closer to edge of yellow png
                    //not sure it is neccessary
                    // js.setMinimumDistance(50);//unknown for now
                    layout_joystick.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View arg0, MotionEvent arg1) {
                            js.drawStick(arg1);
                            return true;
                        }
                    });
                    js.initialiseStickToCenter();
                }
            });
        }
        //  to be accessed inside the ViewTreeObserver
        final MainActivity ma = super.getMainActivity();
        ViewTreeObserverUtility.SetUpViewTreeObserver(getView(), new RunInterface() {
            @Override
            public void run() {
                northWest = (ToggleButton) getView().findViewById(R.id.NorthWest);
                buttonArr[0] = northWest;
                north = (ToggleButton) getView().findViewById(R.id.North);
                buttonArr[1] = north;
                northEast = (ToggleButton) getView().findViewById(R.id.NorthEast);
                buttonArr[2] = northEast;
                east = (ToggleButton) getView().findViewById(R.id.East);
                buttonArr[3] = east;
                southEast = (ToggleButton) getView().findViewById(R.id.SouthEast);
                buttonArr[4] = southEast;
                south = (ToggleButton) getView().findViewById(R.id.South);
                buttonArr[5] = south;
                southWest = (ToggleButton) getView().findViewById(R.id.SouthWest);
                buttonArr[6] = southWest;
                west = (ToggleButton) getView().findViewById(R.id.West);
                buttonArr[7] = west;
                portWing = (RadioButton) getView().findViewById(R.id.RedPortWing);
                brdg = (RadioButton) getView().findViewById(R.id.YellowBrdg);
                stbdWing = (RadioButton) getView().findViewById(R.id.GreenStbdWing);
                middleButtonArr[0] = portWing;
                middleButtonArr[1] = brdg;
                middleButtonArr[2] = stbdWing;

                pitch = (ToggleButton) getView().findViewById(R.id.YellowPitch);
                reset = (ToggleButton) getView().findViewById(R.id.YellowReset);

                if (!ma.getPointOfViewInitilised()) {
                    ma.initialisePointOfViewBool();
                    north.toggle();
                    if (!brdg.isChecked()) {
                        brdg.toggle();
                    }
                }

                //This control the pitch button and joystick when app loads and when returns to this panel from another panel
                if(pitch.isChecked()){
                    turnPitchOn();
                }
                else{
                    turnPitchOff();
                }

            }
        });
    }

    /**
     * Is called everytime the fragment is in view. Will make sure the reset button is turned
     * off and that particular button should not stay on.
     *
     */
    @Override
    public void onResume() {
        super.onResume();
    // ViewTree observer used to check if reset button is on, then turn off if it is
        ViewTreeObserver viewTreeObserver2 = getView().getViewTreeObserver();
        if (viewTreeObserver2.isAlive()) {
            viewTreeObserver2.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    if(reset.isChecked()) {
                        reset.toggle();
                    }
                }
            });
        }
    }

    /**
     * Turns on a button in the array of buttons that controls the cameras direction
     * In turn it will turn off all other buttons in that array
     * @param tb The array of buttons that controls camera direction
     */
    public void turnOnButton(ToggleButton tb) {
        for (ToggleButton b : buttonArr) {
            if (b.isChecked()) {
                b.setChecked(false);
            }
        }
        tb.setChecked(true);
    }

    public static PointOfViewCameraPanelFragment newInstance() {
        PointOfViewCameraPanelFragment fragment = new PointOfViewCameraPanelFragment();
        return fragment;
    }


    /**
     * Calls another method that notifies the main of changes to joysticks progress
     *
     */
    public void joyStickUpdate() {
        notifyObserver();
    }

    /**
     * Tells the main activity that a change has been made to the joystick
     *
     */
    public void notifyObserver() {
        super.getMainActivity().joyStickUpdate();
    }

    //method for button clicks to send signals

    /**
     * Turns on/off the north west button.
     * Sends signal to the server that north west has been pressed
     */
    public void pressPointOfViewNorthWest() {
        turnOnButton(northWest);
        super.getState().sendMessage("pointofview_northwest");
    }
    /**
     * Turns on/off the north button.
     * Sends signal to the server that north has been pressed
     */
    public void pressPointOfViewNorth() {
        turnOnButton(north);
        super.getState().sendMessage("pointofview_north");
    }
    /**
     * Turns on/off the north east button.
     * Sends signal to the server that north east has been pressed
     */
    public void pressPointOfViewNorthEast() {
        turnOnButton(northEast);
        super.getState().sendMessage("pointofview_northeast");
    }
    /**
     * Turns on/off the east button.
     * Sends signal to the server that east has been pressed
     */
    public void pressPointOfViewEast() {
        turnOnButton(east);
        super.getState().sendMessage("pointofview_east");
    }
    /**
     * Turns on/off the south east button.
     * Sends signal to the server that south east has been pressed
     */
    public void pressPointOfViewSouthEast() {
        turnOnButton(southEast);
        super.getState().sendMessage("pointofview_southeast");
    }
    /**
     * Turns on/off the south button.
     * Sends signal to the server that south has been pressed
     */
    public void pressPointOfViewSouth() {
        turnOnButton(south);
        super.getState().sendMessage("pointofview_south");
    }
    /**
     * Turns on/off the south west button.
     * Sends signal to the server that south west has been pressed
     */
    public void pressPointOfViewSouthWest() {
        turnOnButton(southWest);
        super.getState().sendMessage("pointofview_southwest");
    }
    /**
     * Turns on/off the west button.
     * Sends signal to the server that west has been pressed
     */
    public void pressPointOfViewWest() {
        turnOnButton(west);
        super.getState().sendMessage("pointofview_west");
    }
    /**
     * Turns on/off the port wing button.
     * Sends signal to the server that port wing has been pressed
     */
    public void pressPointOfViewRedPortWing() {
        super.getState().sendMessage("pointofview_red_portwing");
    }
    /**
     * Turns on/off the bridge button.
     * Sends signal to the server that bridge has been pressed
     */
    public void pressPointOfViewYellowBrdg() {
        super.getState().sendMessage("pointofview_yellow_brdg");
    }
    /**
     * Turns on/off the std wing button.
     * Sends signal to the server that std wing has been pressed
     */
    public void pressPointOfViewGreenStbdWing() {
        super.getState().sendMessage("pointofview_green_stbdwing");
    }
    /**
     * Turns on/off the pitch button.
     * Sends signal to the server that pitch has been pressed
     */
    public void pressPointOfViewCameraYellowPitch() {
        // this checks if pitch boolean is true or false so we can control the joystick going only up and down or all directions.
        if(js.isPitchOn()){
            turnPitchOff();
        }
        else{
            turnPitchOn();
        }
        super.getState().sendMessage("camera_yellow_pitch");
    }
    /**
     * Turns on reset button, it will only be on for a second.
     * Turns panel into initial state
     * It will turn of all other buttons but leave or turn on  north and brdg
     * Sends signal to the server that reset has been pressed
     */
    public void pressPointOfViewCameraYellowReset() {
        for(ToggleButton tb: buttonArr){
            if(tb.isChecked()){
                tb.setChecked(false);
            }
        }
        for(RadioButton rb: middleButtonArr){
            if (rb.isChecked()) {

                rb.toggle();
            }
        }
        north.toggle();
        brdg.toggle();

        if(pitch.isChecked()){
            turnPitchOff();
        }
        super.getState().sendMessage("camera_yellow_reset");

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                if(reset.isChecked()) {
                    reset.toggle();
                }
            }
        };

        Thread resetThread = new Thread(){

            @Override
            public void run() {


                    Message message = Message.obtain();
                    try {
                        Thread.sleep(1000);
                        message.arg1 = 0;
                        handler.sendMessage(message);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }


                }

        };
        resetThread.start();
    }

    /**
     * Turns on pitch button.
     * Notifies joystick can only work on Y axis
     */
    public void turnPitchOn(){
        pitch.setChecked(true);
        js.turnPitchOn();
    }

    /**
     * turns off pitch button
     * Notifies josytick it can work on X and Y axis
     */
    public void turnPitchOff(){
        pitch.setChecked(false);
        js.turnPitchOff();
    }
    
}